#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pushButton_3->setStyleSheet("QPushButton{border-image:  url(:/images/export.png);}"
                                  "QPushButton:hover{border-image: url(:/images/export_pressed.png);}"
                                  "QPushButton:pressed{border-image: url(:/images/export_pressed.png);margin: 2px;}");
    ui->toolButton_3->setStyleSheet("QToolButton{border-image:  url(:/images/export.png);}"
                                  "QToolButton:hover{border-image: url(:/images/export_pressed.png);}"
                                  "QToolButton:pressed{border-image: url(:/images/export_hover.png);}");
}

MainWindow::~MainWindow()
{
    delete ui;
}
